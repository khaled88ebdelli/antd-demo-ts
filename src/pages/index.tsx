import React from "react";
import LayoutIndex from "../components/Layout";
import { Router, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import routerConfig from "../config/router.config";

const App: React.FC = _props => (
  <Router history={createBrowserHistory()}>
    <LayoutIndex>
      {routerConfig.map(route => (
        <Route
          path={route.path}
          exact={!!route.exact} 
          component={route.component}
        />
      ))}
    </LayoutIndex>
  </Router>
);
export default App;
