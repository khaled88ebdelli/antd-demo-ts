import React from "react";
import { Menu, Icon, List, Avatar } from "antd";
import IconText from "../shared/IconText";
import "./index.scss";

interface Props {}

const Notification: React.FC<Props> = () => {
  return (
    <Menu
      selectedKeys={[]}
      className="app-notifications-group"
      mode="horizontal"
    >
      <Menu.SubMenu key="alert" title={<Icon type="alert" />}>
          <List
            size="small"
            header={<div>Vous avez 3 Nouvelles Notifications</div>}
            footer={<IconText type="small-dash" text="Afficher toutes les Notifications" key="list-vertical-more" />}
            bordered
          >
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar><Icon type="alert" /></Avatar>}
                title="notif 1"
                description="16/12/2020"
              />
            </List.Item>
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar><Icon type="alert" /></Avatar>}
                title="notif 2"
                description="16/12/2020"
              />
            </List.Item>
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar><Icon type="alert" /></Avatar>}
                title="notif 3"
                description="16/12/2020"
              />
            </List.Item>
          </List>
      </Menu.SubMenu>
      <Menu.SubMenu key="file-done" title={<Icon type="file-done" />}>
        <Menu.Item key="file-done:1">Option 1</Menu.Item>
        <Menu.Item key="file-done:2">Option 2</Menu.Item>
      </Menu.SubMenu>
      <Menu.SubMenu key="file-add" title={<Icon type="file-add" />}>
        <Menu.Item key="file-add:1">Option 1</Menu.Item>
        <Menu.Item key="file-add:2">Option 2</Menu.Item>
      </Menu.SubMenu>
      <Menu.Item key="wechat">
        <Icon type="wechat" />
      </Menu.Item>
      <Menu.Item key="logout">
        <Icon type="logout" />
      </Menu.Item>
    </Menu>
  );
};

export default Notification;
