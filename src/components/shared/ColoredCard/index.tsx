import React from "react";
import { Card, Icon } from "antd";

interface Props {
  color: string;
  width?: string;
  height?: string;
  text: string;
  icon: string;
}

const ColoredCard: React.FC<Props> = ({ color, width = "100%", height = "180px", text, icon }) => {
  return (
    <Card
      style={{
        width: width,
        height: height,
        color: "white",
        backgroundColor: color,
        textAlign: "center",
        padding: "15px",
        fontSize: "15px",
        borderRadius: "7px"
      }}
      cover={<Icon style={{ fontSize: "100px" }} type={icon} />}
    >
      <Card.Meta title={text} />
    </Card>
  );
};

export default ColoredCard;
