import React from "react";
import ColoredCard from "../shared/ColoredCard";
import "./index.scss";
interface Props {}

const Dashboard: React.FC<Props> = () => {
  return (
    <div className="grid">
      <ColoredCard text="text 1" color="#efbf2f" icon="folder-add" />
      <ColoredCard text="text 2" color="#65b145" icon="file-add" />
      <ColoredCard text="text 3" color="#bf3846" icon="check-circle" />
      <ColoredCard text="text 4" color="#5495bf" icon="team"/>
      <ColoredCard text="text 5" color="#8767b0" icon="calendar"/>
    </div>
  );
};

export default Dashboard;
