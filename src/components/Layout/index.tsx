import React from "react";
import { Layout  } from 'antd';
import Sidebar from "./SideBar";
import Header from "./Header";

const { Content } = Layout;
interface Props {}
const LayoutIndex: React.FC<Props> = ({children}) => { 
  return (
    <Layout>
      <Sidebar/>
      <Layout>
        <Header/>
        <Content style={{ padding: '20px'}}>{children}</Content>
        {/* <Footer/> */}
      </Layout>
    </Layout>
  );
}
export default LayoutIndex;
