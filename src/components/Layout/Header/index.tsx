import React from "react";
import { Layout, Input } from "antd";
import { withRouter } from "react-router-dom";
import { History } from "history";

import "./index.scss";
import Notification from "../../Notification";
const { Header } = Layout;
const { Search } = Input;

interface Props {
  history: History;
}
const MainHeader: React.FC<Props> = ({ history }) => {
  return (
    <Header className="app-header">
      <Search
        placeholder="Recherche..."
        onSearch={value => console.log(history)}
      />
      <Notification/>
    </Header>
  );
};

export default withRouter(MainHeader);
