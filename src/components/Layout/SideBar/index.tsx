import React from "react";
import { Layout, Menu, Icon, Divider } from "antd";
import { History } from "history";
import logo from "./../../../assets/logo.png";
import "./index.scss";

const { Sider } = Layout;
const { SubMenu } = Menu;

interface Props {
  history?: History;
}
const Sidebar: React.FC<Props> = ({ history }) => {
  console.log(history);
  return (
    <Sider width={220} style={{ padding: "10px", marginRight: "10px" }}>
      <img src={logo} className="logo" alt="lunatik logo" />
      <Divider style={{ width: "90%", textAlign: 'center' }} />
      <Menu
        mode="inline"
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["dashboard"]}
        style={{ height: "100%", borderRight: 0 }}
      >
        <SubMenu
          key="dashboard"
          title={
            <span>
              <Icon type="dashboard" />
              Tableau de bord
            </span>
          }
        >
          <Menu.Item key="1">Nouveau Budget</Menu.Item>
          <Menu.Item key="2">Budgets</Menu.Item>
          <Menu.Item key="3">En Cours</Menu.Item>
          <Menu.Item key="4">A Venir</Menu.Item>
          <Menu.Item key="5">Cloturés</Menu.Item>
          <Menu.Item key="6">Archivés</Menu.Item>
          <Menu.Item key="7">Ajouter Briefs</Menu.Item>
          <Menu.Item key="8">Check Point</Menu.Item>
        </SubMenu>
        <SubMenu
          key="teamManagement"
          title={
            <span>
              <Icon type="team" />
              Team Management
            </span>
          }
        >
          <Menu.Item key="9">Gérer les profils</Menu.Item>
          <Menu.Item key="10">Dispatch équipe</Menu.Item>
        </SubMenu>
        <SubMenu
          key="agenda"
          title={
            <span>
              <Icon type="notification" />
              Agenda
            </span>
          }
        >
          <Menu.Item key="13">Budgets En Cours</Menu.Item>
          <Menu.Item key="14">Budgets à Venir</Menu.Item>
          <Menu.Item key="15">option11</Menu.Item>
          <Menu.Item key="16">option12</Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  );
};
export default Sidebar;
