import Login from "../components/Login";
import Dashboard from "../components/Dashboard";
export default [
  // user
  {
    path: "/login",
    component: Login,
    authority: ["anonymous"]
  },
  // app
  {
    path: "/",
    component: Dashboard,
    authority: ["super-admin", "admin", "user"],
    exact: true
  }
];
