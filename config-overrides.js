const { override, fixBabelImports, addLessLoader } = require("customize-cra");
const darkTheme = require('./src/config/theme.config');

//console.log(darkTheme);

module.exports = override(
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {...darkTheme, "@background-color": "#292a2e" }
  })
);